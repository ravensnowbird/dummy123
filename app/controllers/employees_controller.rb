class EmployeesController < ApplicationController
  before_action :authenticate_employee!, only: [:listing, :new, :create]
  before_action :check_role, only: [:new, :create]

  def listing
    @employees = Employee.all
  end

  def new
    puts current_employee.inspect
  end

  def create 
    puts params
    @employee = Employee.new
    @employee.name = params[:name]
    @employee.email = params[:email]
    @employee.employee_id = params[:employee_id]
    @employee.password = "000000"

    if @employee.save
      puts @employee.inspect
      redirect_to "/"
    else
      puts @employee.errors.inspect
      redirect_to "/employees/new"
    end
  end

  def edit_employee
    @employee = Employee.where(id: params[:sadsad]).first
  end

  def update_employee
    @employee = Employee.where(id: params[:sadsad]).first
      
    @employee.name = params[:name]
    @employee.email = params[:email]
    @employee.employee_id = params[:employee_id]

    if @employee.save
      redirect_to "/"
    else 
      redirect_to "/employees/#{@employee.id}/edit"
    end


  end

  private

    def check_role
      if current_employee.role != "Admin"
        redirect_to "/"
      end
    end
end
Rails.application.routes.draw do
  devise_for :employees
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: "employees#listing"

  get "/employees/new" => "employees#new"

  post 'employees/create' => 'employees#create'

  get "/employees/:sadsad/edit" => "employees#edit_employee"

  post '/employees/:sadsad/update_employee' => "employees#update_employee"

end

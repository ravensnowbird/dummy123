class AddCreatedByRoleToEmployees < ActiveRecord::Migration[6.1]
  def change
    add_column :employees, :created_by, :integer
    add_column :employees, :role, :string, default: "Employee"
  end
end
